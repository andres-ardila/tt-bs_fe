import {Component, OnInit} from '@angular/core';
// import {AuthService, GoogleLoginProvider, SocialUser} from 'angularx-social-login';
import {ActivatedRoute, Router} from '@angular/router';
import {GoogleApiService, GoogleAuthService} from 'ng-gapi';
import {UserService} from '../../services/user.service';

declare const gapi: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {

  // user: SocialUser;
  loggedIn: boolean;

  constructor(// private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private authService: GoogleAuthService,
    private gapiService: GoogleApiService,
    private userService: UserService) {
    this.gapiService.onLoad().subscribe();
  }

  ngOnInit() {
    // this.authService.authState.subscribe((user) => {
    //   this.user = user;
    //   this.loggedIn = (user != null);
    //   // console.log(this.user);
    // });
    this.route.fragment.subscribe((fragment) => {
      console.log(fragment);
    });
  }

  public isLoggedIn(): boolean {
    return this.userService.isUserSignedIn();
  }

  public signIn() {
    this.userService.signIn();
    console.log('isLoggedIn:', this.userService.isUserSignedIn());
    /*if (this.isLoggedIn()) {
      this.router.navigate([`/dashboard`]);
    }*/
  }

  public signOut() {
    sessionStorage.clear();
    this.userService.signOut();
  }

  // signIn(): void {
  //   this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then(user => {
  //     // console.log(user);
  //
  //     sessionStorage.setItem('user', JSON.stringify(user));
  //     // console.log('user:', JSON.parse(sessionStorage.getItem('user')));
  //
  //     this.router.navigate([`/dashboard`]);
  //   });
  // }
}
