import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AnalyticsReportsService {

  private resource = 'https://analyticsreporting.googleapis.com/v4/reports:batchGet';

  constructor(private httpClient: HttpClient) {
  }

  public getGAnalyticsMetrics(reportRequestsBody: any, token: string): Observable<any> {

    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + token
      })
    };

    return this.httpClient.post<any>(this.resource, reportRequestsBody, httpOptions);
  }

  /*public saveCategory(category: SignatureCategoryModel): Observable<FinalReply> {

    return this.httpClient.post<FinalReply>(SIGNATURE_CATEGORIES_EP, category);
  }*/
}
