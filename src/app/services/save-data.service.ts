import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SaveDataService {

  private resource = environment.apiUrl;

  constructor(private httpClient: HttpClient) {
  }

  public saveUserData(userData: string): Observable<any> {

    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: 'Basic ' + environment.token
      })
    };

    return this.httpClient.post<any>(this.resource + 'user', userData, httpOptions);
  }

  public saveGAnalyticsMetrics(userData: string): Observable<any> {

    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: 'Basic ' + environment.token
      })
    };

    return this.httpClient.post<any>(this.resource + 'metrics', userData, httpOptions);
  }
}
