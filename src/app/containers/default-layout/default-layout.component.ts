import {Component, OnInit} from '@angular/core';
import {navItems} from '../../_nav';
// import {AuthService, SocialUser} from 'angularx-social-login';
import {Router} from '@angular/router';
import {GoogleAuthService} from 'ng-gapi';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent implements OnInit {
  public sidebarMinimized = false;
  public navItems = navItems;

  user: any;
  loggedIn: boolean;

  constructor(
    private userService: UserService
    /*private authService: AuthService*/) {
  }

  ngOnInit() {
    /*this.authService.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = (user != null);
      // console.log(this.user);
    });*/
    this.user = JSON.parse(sessionStorage.getItem('user'));
    console.log('user:', this.user);
  }

  public signOut() {
    sessionStorage.clear();
    this.userService.signOut();
  }

  /*signOut(): void {
    sessionStorage.clear();
    // this.authService.signOut(true);
  }*/

  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }
}
